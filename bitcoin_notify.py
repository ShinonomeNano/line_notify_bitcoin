"""
    Get bitcoin price and notify in Line group.
    Language: Thai
    Ref: https://coins.co.th/
"""
import urllib.parse
import time

import certifi
import requests
import urllib3

# Paste your Line notify here:
TOKEN = 'Your Token Here'


def get_html_page():
    """Get Html source code.

    Returns:
        [string] -- [Html code]
    """

    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
    response = http.request('GET', 'https://coins.co.th/')
    page_source = response.data.decode('utf-8')

    return page_source


def find_bitcoin_price(html_code):
    """Get bitcoin price, buy and sell.

    Arguments:
        html_code {[string]} -- [Html code]

    Returns:
        [dictionary] -- [buy and sell price dictinary]
    """

    buy_price = html_code[html_code.find('B:') + 26:]
    buy_price = buy_price[:buy_price.find(' THB')]

    sell_price = html_code[html_code.find('S:') + 26:]
    sell_price = sell_price[:sell_price.find(' THB')]

    price_dict = {'buy': buy_price, 'sell':sell_price}

    return price_dict


def line_notify(price_dict):
    """
    docstring here
        :param price_dict:
    """

    url = 'https://notify-api.line.me/api/notify'

    message = "ราคา Bitcoin ปัจจุบัน\nซื้อ : {0} บาท\nขาย : {1} บาท"\
                .format(price_dict['buy'], price_dict['sell'])
    msg = urllib.parse.urlencode({"message": message})
    line_heads = {'Content-Type':'application/x-www-form-urlencoded',
                  "Authorization":"Bearer " + TOKEN}
    session = requests.Session()
    send = session.post(url, headers=line_heads, data=msg)

    return send.text


def run(delay=60):
    """[summary]

    Keyword Arguments:
        delay {[int]} -- [Delay time (second)] (default: {60 Second})

    Returns:
        [int] -- [No return]
    """

    html = get_html_page()
    price_dict = find_bitcoin_price(html)
    line_notify(price_dict)

    time.sleep(delay)
    return 0

if __name__ == '__main__':
    while True:
        try:
            run(300)
        except RuntimeError:
            pass
